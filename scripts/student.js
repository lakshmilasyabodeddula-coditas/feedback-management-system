export const getAndDisplayStudents=async(event,trainers)=>{
    let trainerId1=event.target.id;
    for(let trainer of trainers)
    {
        if(trainer.trainerId==trainerId1)
        {
            const array= trainer.students;
            return array;
        }
    }
}