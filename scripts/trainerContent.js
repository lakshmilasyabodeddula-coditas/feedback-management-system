import { $ } from "./dom.js";
import http from "./http.js";
import {trainerRow} from"./components/trainerRow.js";
import {getAndDisplayStudents} from "./student.js";
import {studenttabletemplate} from "./components/studentTableTemplate.js";
import {studentrow} from "./components/studentrow.js";


const trainerTableTemplate=`<table id="trainertable">
                                <thead>
                                    <th>trainerId</th>
                                    <th>trainerName</th>
                                    <th>trainerEmail</th>
                                    <th>trainerSalary</th>
                                    <th>domainId</th>
                                    <th>domainName</th>
                                    <th><button id="create-trainer">create</button></th>
                                </thead>
                                <tbody id="trainer-table-body"></tbody>
                            </table>`

                    
export const getandDisplayTrainerDetails=async()=>{
    const trainers=await http.get("admin/getTrainers");
    let trainerrow=``;
    $(".dashboard-content")[0].innerHTML=trainerTableTemplate;
    for(let trainer of trainers)
    {
        trainerrow=trainerrow+ trainerRow(trainer);
    }
   $("#trainer-table-body").innerHTML=trainerrow;
   $("#trainertable").addEventListener("click",async (event)=>{
         let selctedClass=event.target.className;
         if(selctedClass==="viewstudent")
         {
            const students= await getAndDisplayStudents(event,trainers);
            let newrow=``;
             for(let student of students)
             {
                newrow=newrow+studentrow(student);
             }
            const divvie=document.createElement("div");
             document.body.append(divvie);
            divvie.innerHTML=studenttabletemplate;
            $("#student-body").innerHTML=newrow;
         }

   });
}
