export const studentDetails=(student)=>{
    return `
            <section class="student-container">
            <h2>Student Details</h2>
            <div>
                <span>Name</span>
                <span>${student.studentId}</span>
            </div>
            <div>
                <span>studentName</span>
                <span>${student.studentName}</span>
            </div>
            <div>
                <span>studentEmail</span>
                <span>${student.studentEmail}</span>
            </div>
            <div>
                <span>domainName</span>
                <span>${student.studentDomain.domainName}</span>
            </div>
            </section`

}