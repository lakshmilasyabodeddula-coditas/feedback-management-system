import { $ } from "./dom.js"
import {adminpage} from "./components/adminpagetemplate.js";
import {getandDisplayTrainerDetails} from "./trainerContent.js";
import { getStudents } from "./adminStudent.js";
import {getandDisplayFeedback} from "./"

const adminPage=()=>{
     $("#root").innerHTML=adminpage;
     $("#trainer-btn").addEventListener("click",getandDisplayTrainerDetails);
     $("$student-btn").addEventListener("click",getStudents);
     $("#feedback-trainer-btn").addEventListener("click",getandDisplayFeedback);
}
export default{
    adminPage
}