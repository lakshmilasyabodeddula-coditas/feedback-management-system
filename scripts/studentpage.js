import {studentpagerow} from "./components/studentpagerow.js";
import {$} from "./dom.js";

import http from "./http.js"

let studenttabletemplate=`<table id="student-table">
                                <thead>
                                    <th>studentId</th>
                                    <th>studentName</th>
                                    <th>studentEmail</th>
                                    <th>domain id</th>
                                    <th>domainId</th>
                                    <th>domainName</th>
                                </thead>
                                <tbody id="studentTableBody"></tbody>
                           </table>`
export const studentContent=async(id)=>{
    let students=await http.get(`student/getStudent/${id}`);
    console.log(students);
    let row=studentpagerow(students);
    studentDetails=document.createElement("section");
    studentDetails.classList.add("studentPage")
    $("#root").innerHTML=studentDetails;
    $("#studentTableBody").innerHTML=row;
    studentDetails.innerHTML=studenttabletemplate;
}