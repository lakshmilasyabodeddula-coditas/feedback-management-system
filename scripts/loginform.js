import {loginFormTemplate} from "./components/loginformtemplate.js";
import { $ } from "./dom.js";
import http from "./http.js";
import adminpage from "./adminpage.js";
import {studentContent} from "./studentpage.js"
import { trainerPage } from "./trainerPage.js";

const createLoginForm=()=>{
    const loginForm=loginFormTemplate;
     $("#root").innerHTML=loginForm;
     $("#login-form-submit-button").addEventListener("click",authenticateForm);
}

const authenticateForm=async(event)=>{
            event.preventDefault();
            const data={
                email:$("#username").value,
                password:$("#password").value
                }
            const authenticatedResponse= await http.post("authenticate",data);
            let role=authenticatedResponse.userRole;

            if(role==="ADMIN")
            {
            $(".form-holder")[0].style.display="none";
            adminpage['adminPage']();
            }
            else if(role==="STUDENT")
            {
            $(".form-holder")[0].style.display="none";   
             let studentId=authenticatedResponse.userId;
             studentContent(studentId);
            }
            else if(role==="TRAINER")
            {
                $(".form-holder")[0].style.display="none"
                let trainerId=authenticatedResponse.userId;
                trainerPage(trainerId);
            }
}


export default {
authenticateForm,
createLoginForm
}