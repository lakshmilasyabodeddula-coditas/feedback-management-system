import { trainerRow } from "./components/trainerRow.js";
import { $ } from "./dom.js";
import http from "./http.js"
import {trainertablerow} from "./components/trainertablerow.js";
import {trainerFeedback} from "./components/trainerfeedback.js";

let trainerId1=0;
let trainerpagetemplate=`<section class="trainer-page">
                            <section class="trainer-dashboard-pointer">
                                    <div class="view-profile">
                                        <button>Your profile</button>
                                    </div>

                                    <div class="view-feedback">
                                        <button>View Feed Back</button>
                                    </div>
                                    <div class="view-rating">
                                        <button>view rating</button>
                                    </div>
                            </section>
                            <section class="content-viewer">
                            </section>
                        </section>`


export const trainerPage= (trainerId)=>{
   trainerId1=trainerId;
    $("#root").innerHTML=trainerpagetemplate;
    $(".view-profile")[0].addEventListener("click", displayTrainerDetails);
    $(".view-feedback")[0].addEventListener("click",displayFeedback);
    $(".view-rating")[0].addEventListener("click",displayaveragerating);
}                                                               

const displayTrainerDetails=async()=>{
    const trainer=await http.get(`trainer/getTrainer/${trainerId1}`);
    const trainerProfile= trainertablerow(trainer);
    $(".content-viewer")[0].innerHTML=trainerProfile;
}

const displayFeedback=async(trainerId)=>{
    const feedbacks=await http.get(`trainer/getFeedback/${trainerId1}`);
    let feedbacktext=``;
    for(let feedback of feedbacks)
    {
    feedbacktext=feedbacktext+trainerFeedback(feedback);
    }
    $(".content-viewer")[0].innerHTML=feedbacktext;
}

const displayaveragerating=async(trainerId)=>{
    const averageRating=await http.get(`trainer/getFeedbackRating/${trainerId1}`);
    const rating=document.createElement("div");
    rating.classList.add("rating-text");
    rating.innerHTML=averageRating;
    $(".content-viewer")[0].append(rating)
}
