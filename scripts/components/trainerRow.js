export const trainerRow=(trainer)=>{
    return `
        <tr>
            <td>${trainer.trainerId}</td>
            <td>${trainer.trainerName}</td>
            <td>${trainer.trainerEmail}</td>
            <td>${trainer.trainerSalary}</td>
            <td>${trainer.domain.domainId}</td>
            <td>${trainer.domain.domainName}</td>
            <td><button id="${trainer.trainerId}" class="viewstudent">VIEW STUDENTS</button></td>
            <td><button class="edit-trainer" id="${trainer.trainerId}">Edit</button></td>
            <td><button class="delete-trainer" id="${trainer.trainerId}">Delete</button></td>
        </tr>
    `   
}