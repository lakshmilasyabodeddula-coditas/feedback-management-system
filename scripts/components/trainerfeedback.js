export const trainerFeedback=(feedback)=>
{
    console.log(feedback);

    return `<section class="feedback-container"> 
            <h2>Feed Back details</h2>
                        <div>
                            <span>studentId :</span>
                            <span>${feedback.studentId}</span>
                        </div>
                        <div>
                            <span>studentName :</span>
                            <span>${feedback.studentName}</span>
                        </div>
                        <div>
                            <span>feedbackRating :</span>
                            <span>${feedback.feedbackRating}</span>
                        </div>
                        <div>
                            <span>feedbackComment :</span>
                            <span>${feedback.feedbackComment}</span>
                        </div>
                        </section>
                        `
}
