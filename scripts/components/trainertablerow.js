export const trainertablerow=(trainer)=>{
    return `
              <section class="trainer-profile">  
             <h2>YOUR PROFILE</h2>
              <p><span>TRAINER ID:${trainer.trainerId} </span></p>
              <p><span>TRAINER NAME:${trainer.trainerName}</span></p>
              <p><span>TRAINER EMAIL:${trainer.trainerEmail}</span></p>
              <p><span>TRAINER SALARY:${trainer.trainerSalary}</span></p>
              <p><span>TRAINER DOMAIN ID:${trainer.domain.domainId}</span></p>
              <p><span>TRAINER DOMAIN NAME:${trainer.domain.domainName}</span></p>
              </section>
            `
}